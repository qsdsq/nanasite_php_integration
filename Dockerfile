FROM php:8.2-apache

# Copier les fichiers dans le répertoire /var/www/html/
COPY . /var/www/html/

# Installer l'extension mysqli
RUN docker-php-ext-install mysqli

# Activer le module rewrite pour Apache
RUN a2enmod rewrite

# Donner des permissions d'exécution à phpunit
RUN chmod +x /var/www/html/vendor/bin/phpunit
